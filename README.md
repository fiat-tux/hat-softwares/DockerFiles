This is the repository of my DockerFiles.

The docker images are published under the `hatsoftwares` account on the [Docker store](https://store.docker.com/profiles/hatsoftwares).

All images are Debian-based.

* [test-ci](https://store.docker.com/community/images/hatsoftwares/test-ci) is used for my softwares tests
* [gitbook](https://store.docker.com/community/images/hatsoftwares/gitbook) provide an environment with [Gitbook](https://www.gitbook.com/)
* [vnu](https://store.docker.com/community/images/hatsoftwares/vnu) contains the [Nu Html Checker](https://validator.github.io/validator/), which provides Html checks like the W3C validator
* [texlive-full](https://store.docker.com/community/images/hatsoftwares/texlive-full) provides an utf8-ready texlive environment
